rand [![GoDoc](http://godoc.org/gitlab.com/opennota/rand?status.svg)](http://godoc.org/gitlab.com/opennota/rand) [![License](http://img.shields.io/:license-gpl3-blue.svg)](http://www.gnu.org/licenses/gpl-3.0.html) [![Pipeline status](https://gitlab.com/opennota/rand/badges/master/pipeline.svg)](https://gitlab.com/opennota/rand/commits/master)
====

`rand` package provides a simple deterministic pseudo-random number generator (PRNG) based on SHA-512.

## Install

    go get -u gitlab.com/opennota/rand

## Test

To run the [dieharder](https://www.phy.duke.edu/~rgb/General/dieharder.php) testing suite, issue the following command:

    go test -tags dieharder -timeout 24h

## Use

``` go
rnd := rand.New(12345 /* random salt */)

// generate and print pseudo-random 64-bit number
fmt.Println(rnd.Uint64())

// generate and print 5 pseudo-random numbers in range [0, 10).
for i := 0; i < 5; i++ {
	fmt.Println(rnd.Uint64n(10))
}
```
