package main

import (
	"fmt"

	"gitlab.com/opennota/rand"
)

func main() {
	rnd := rand.New(12345 /* random salt */)

	// generate and print pseudo-random 64-bit number
	fmt.Println(rnd.Uint64())

	// generate and print 5 pseudo-random numbers in range [0, 10).
	for i := 0; i < 5; i++ {
		fmt.Println(rnd.Uint64n(10))
	}
}
