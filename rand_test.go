package rand

import (
	"crypto/sha512"
	"encoding/hex"
	"reflect"
	"testing"
)

func TestSHA512(t *testing.T) {
	hash := sha512.New()
	hash.Write([]byte("12345"))
	const want = "3627909a29c31381a071ec27f7c9ca97726182aed29a7ddd2e54353322cfb30abb9e3a6df2ac2c20fe23436311d678564d0c8d305930575f60e2d3d048184d79"
	got := hex.EncodeToString(hash.Sum(nil))
	if got != want {
		t.Errorf("\nwant %s\n got %s", want, got)
	}
}

func TestUint64(t *testing.T) {
	rnd := New(0)
	var got []uint64
	for i := 0; i < 10; i++ {
		got = append(got, rnd.Uint64())
	}
	var want = []uint64{
		16607377186109647227,
		1257350251931989237,
		5272478156957453960,
		18114923214192731967,
		8875543887232929618,
		18258178713192914904,
		9674395462357576766,
		2892496166146977117,
		8511019353553860010,
		1057899734115447976,
	}
	if !reflect.DeepEqual(got, want) {
		t.Errorf("Uint64:\nwant %v\n got %v", want, got)
	}
}

func TestUint64n(t *testing.T) {
	rnd := New(12345)
	var got []uint64
	for i := 0; i < 20; i++ {
		got = append(got, rnd.Uint64n(10))
	}
	var want = []uint64{7, 0, 1, 8, 3, 4, 8, 8, 1, 7, 1, 6, 4, 4, 7, 8, 0, 9, 2, 0}
	if !reflect.DeepEqual(got, want) {
		t.Errorf("Uint64n:\nwant %v\n got %v", want, got)
	}
}
