// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

// Package rand provides simple deterministic PRNG based on SHA-512.
package rand

import (
	"crypto/sha512"
	"encoding/binary"
)

// A Rand is a source of random numbers.
type Rand struct {
	buf [sha512.Size]byte
}

// New returns a new seeded Rand.
func New(seed uint64) *Rand {
	rnd := Rand{}
	binary.LittleEndian.PutUint64(rnd.buf[:], seed)
	return &rnd
}

// Uint64 returns a pseudo-random 64-bit value.
func (r *Rand) Uint64() uint64 {
	hash := sha512.New()
	hash.Write(r.buf[:])
	copy(r.buf[:], hash.Sum(nil))
	return binary.LittleEndian.Uint64(r.buf[:])
}

// Uint64n returns a non-negative pseudo-random number in [0, n).
func (r *Rand) Uint64n(n uint64) uint64 {
	if n <= 0 {
		panic("invalid argument to Uint64n")
	}
	if n&(n-1) == 0 { // n is a power of 2, can mask
		return r.Uint64() & (n - 1)
	}
	// avoid bias: ignore numbers outside of [0, K),
	// where K is a max. 64-bit number such that K % n == 0.
	const maxUint64 = 0xffffffffffffffff
	max := maxUint64 - maxUint64%n - 1
	v := r.Uint64()
	for v > max {
		v = r.Uint64()
	}
	return v % n
}
