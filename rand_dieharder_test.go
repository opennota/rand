// +build dieharder

package rand

import (
	"encoding/binary"
	"os"
	"os/exec"
	"testing"
	"time"
)

func TestDieHarderUint64(t *testing.T) {
	cmd := exec.Command("dieharder", "-a", "-g", "200")
	stdin, err := cmd.StdinPipe()
	if err != nil {
		t.Fatal(err)
	}

	cmd.Stdout = os.Stdout
	cmd.Start()
	prng := New(uint64(time.Now().Unix()))
	stopChan := make(chan struct{})
	go func() {
		buf := make([]byte, 8)
		for {
			select {
			case <-stopChan:
				return
			default:
				binary.LittleEndian.PutUint64(buf, prng.Uint64())
				stdin.Write(buf)
			}
		}
	}()

	err = cmd.Wait()
	if err != nil {
		t.Errorf("Got error %q; do you have dieharder installed?", err)
	}

	close(stopChan)
	stdin.Close()
}
